const testArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const testSum = 15;

const numberIdentifier = (collection, sum) => {
  let answer = "No";

  for (let c of collection) {
    const index = collection.indexOf(c);
    const fixedCollection = collection.slice(index, index + 2);

    const total = fixedCollection[0] + fixedCollection[1];

    if (total === sum) {
      return (answer = `OK, matching pair (${fixedCollection[0]},${fixedCollection[1]})`);
    }
  }

  return answer;
};

console.log({ result: numberIdentifier(testArr, testSum) });

module.exports = numberIdentifier;
