/* eslint-disable no-undef */
const numberIdentifier = require("../index");

describe("numberIdentifier", () => {
  it("Should return 'OK' and the pair, when the matching pair is equal to the sum", () => {
    const testArray = [1, 2, 3, 4];
    const testSum = 7;

    expect(numberIdentifier(testArray, testSum)).toContain("OK");
  });

  it("Should return 'No' when the matching pair is NOT equal to the sum", () => {
    const testArray = [1, 2, 3, 4];
    const testSum = 99;

    expect(numberIdentifier(testArray, testSum)).toContain("No");
  });
});
